import matplotlib.pyplot as plt
import math

R = 8.3144598 # m3 Pa K-1 mol-1

def n_pVT(p, V, T):
	return p*V/(R*T)

def p_nVT(n, V, T):
	return n*R*T/V

def psi_to_pa(psi):
	return psi*6894.76

def pa_to_psi(pa):
	return pa/6894.76

def force_to_kg(force):
	return force/9.8

class Spring(object):
	def __init__(self, l_t, l_e, A_p, A_n, V_p, V_n):
		self.l_t = l_t
		self.l_e = l_e
		self.A_p = A_p
		self.A_n = A_n
		self.V_p = V_p
		self.V_n = V_n

	def volume_p(self, l):
		return self.V_p + self.A_p*(self.l_t - l)

	def volume_n(self, l):
		return self.V_n + self.A_n*l

	def pressure_p(self, l):
		return p_nVT(self.n_p, self.volume_p(l), self.T)

	def pressure_n(self, l):
		return p_nVT(self.n_n, self.volume_n(l), self.T)

	def pressurise(self, p, T):
		self.T = T
		self.n_p = n_pVT(p, self.volume_p(0), self.T)
		self.n_n = n_pVT(self.pressure_p(self.l_e),
				self.volume_n(self.l_e), self.T)

	def set_temperature(T):
		self.T = T

	def force(self, l):
		assert(0 <= l <= self.l_t)
		return self.A_p*self.pressure_p(l) - self.A_n*self.pressure_n(l)

def integrate(x, y):
	z = [0]
	for i in range(1, len(x)):
		z.append(z[i-1] + (x[i] - x[i-1])*(y[i] + y[i-1])/2)
	return z

l_t = 0.150
l_e = 0.005
r_p = 0.032/2
r_rod = 0.005
A_p = math.pi*r_p**2
A_n = math.pi*r_p**2 - math.pi*r_rod**2
T = 293

V_token = math.pi*r_p**2*0.015

pnts = 100
ls = [l_t*i/(pnts - 1) for i in range(pnts)]

fig = plt.figure()
ax = fig.add_subplot(2, 2, 1)
#ax2 = ax.twinx()
ax2 = fig.add_subplot(2, 2, 2)
ax.set_xlabel('Stroke (m)')
ax.set_ylabel('Force (N)')
ax2.set_ylabel('Energy (J)')
for psi in [60, 65, 70, 75, 80, 85]:
	spring = Spring(l_t, l_e, A_p, A_n,
			V_p=A_p*0.080,
			V_n=A_n*0.020)
	spring.pressurise(psi_to_pa(psi), T)
	force = [spring.force(l) for l in ls]
	energy = integrate(ls, force)
	lab = 'T0 '+str(psi)
	ax.plot(ls, force, label=lab)
	ax2.plot(ls, energy, label=lab)
ax.legend(loc=2)
ax2.legend(loc=2)

ax = fig.add_subplot(2, 2, 3)
#ax2 = ax.twinx()
ax2 = fig.add_subplot(2, 2, 4)
ax.set_xlabel('Stroke (m)')
ax.set_ylabel('Force (N)')
ax2.set_ylabel('Energy (J)')
for tokes in [0, 1, 2, 3, 4]:
	spring = Spring(l_t, l_e, A_p, A_n,
			V_p=A_p*0.080 - V_token*tokes,
			V_n=A_n*0.020)
	spring.pressurise(psi_to_pa(70), T)
	force = [spring.force(l) for l in ls]
	energy = integrate(ls, force)
	lab = 'T'+str(tokes)+' 70'
	ax.plot(ls, force, label=lab)
	ax2.plot(ls, energy, label=lab)
ax.legend(loc=2)
ax2.legend(loc=2)

plt.show()
